//
//  DealModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 21/12/20.
//

import Foundation
import SwiftyJSON

class DealModel {
    
    var deal_id = ""
    var store_id = ""
    var minimum_discount = ""
    var maximum_discount = ""
    var deal_name = ""
    var deal_image = ""
    var view_count = ""
    var description = ""
    var deal_type = ""
    var deal_special = ""
    var from_special = ""
    var end_date = ""
    var is_valid = ""
    var start_date = ""
    var person_avail =  ""
    var image = ""
    
    func parse(_ details: JSON) {
        
        deal_id = details["deal_id"].string ?? ""
        store_id = details["store_id"].string ?? ""
        minimum_discount = details["minimum_discount"].string ?? ""
        maximum_discount = details["maximum_discount"].string ?? ""
        
        deal_name = details["deal_name"].string ?? ""
        deal_image = details["deal_image"].string ?? ""
        view_count = details["view_count"].string ?? ""
        
        description = details["description"].string ?? ""
        deal_type = details["deal_type"].string ?? ""
        deal_special = details["deal_special"].string ?? "0"
        from_special = details["from_special"].string ?? ""
        end_date = details["end_date"].string ?? ""
        is_valid = details["is_valid"].string ?? ""
        start_date = details["start_date"].string ?? ""
        person_avail = (details["person_avail"].string == "" ? "0" : details["person_avail"].string)!
        image = details["image"].string ?? ""
        
        
        
        
    }
}
