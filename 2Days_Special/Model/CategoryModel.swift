//
//  CategoryModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 29/12/20.
//

import Foundation


import Foundation
import SwiftyJSON

class CategoryModel: NSObject {
    
   
    var id = ""
    var name = ""
    
  
    
     func parse(_ details: JSON) {
        
        id = details["id"].string ?? ""
        name = details["name"].string ?? ""

    }
}
