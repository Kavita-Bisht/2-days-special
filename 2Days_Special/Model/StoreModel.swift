//
//  StoreModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 29/12/20.
//

import Foundation
import  Alamofire
import SwiftyJSON


class StoreModel: NSObject {
    
    var store_id = ""
    var store_name = ""
    var store_email = ""
    var store_phone = ""
    var store_address = ""
    var store_description = ""
    var deals: [DealModel] = []
    var coupons: [DealModel] = []
    
    
     func parse(_ details: JSON) {
        
        store_id = details["store_id"].string ?? ""
        store_name = details["store_name"].string ?? ""
        store_email = details["store_email"].string ?? ""
        store_address = details["store_address"].string ?? ""
        store_phone = details["store_phone"].string ?? ""
        store_address = details["store_description"].string ?? ""
        
        for i in details["deals"].arrayValue {
            let deal = DealModel()
            deal.parse(i)
            deals.append(deal)
        }
        
        for i in details["coupons"].arrayValue {
            let deal = DealModel()
            deal.parse(i)
            coupons.append(deal)
        }
        
    }
}
