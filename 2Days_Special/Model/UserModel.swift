//
//  UserModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 17/12/20.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {
    
    static var sharedInstance = UserModel()
    
    var user_id = ""
    var account_type = ""
    var email = ""
    var facebook_id = ""
    var google_id = ""
    var linkedin_id = ""
    var name = ""
    var gender = ""
    var dob = ""
    var image = ""
    var phone = ""
    var country = ""
    var access_token = ""
    var user_type = ""
    
    
    class func parse(_ details: JSON) {
        
        if let user_id = details["user_id"].string {
            UserModel.sharedInstance.user_id = user_id
            UserDefaults.standard.set(user_id, forKey: "user_id")
        }
        
        if let account_type = details["account_type"].string {
            UserModel.sharedInstance.account_type = account_type
            UserDefaults.standard.set(account_type, forKey: "account_type")
        }
        
        if let email = details["email"].string {
            UserModel.sharedInstance.email = email
            UserDefaults.standard.set(email, forKey: "email")
        }
        
        if let name = details["name"].string {
            UserModel.sharedInstance.name = name
            UserDefaults.standard.set(name, forKey: "name")
        }
        
        if let phone = details["phone"].string {
            UserModel.sharedInstance.phone = phone
            UserDefaults.standard.set(phone, forKey: "phone")
        }
        
        if let access_token = details["access_token"].string {
            UserModel.sharedInstance.phone = access_token
            UserDefaults.standard.set(access_token, forKey: "access_token")
        }
        if let access_token = details["user_type"].string {
            UserModel.sharedInstance.phone = access_token
            UserDefaults.standard.set(access_token, forKey: "user_type")
        }
        
        UserDefaults.standard.synchronize()
    }
}
