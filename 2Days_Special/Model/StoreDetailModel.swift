//
//  StoreDetailModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 29/12/20.
//

import Foundation
import SwiftyJSON

class StoreDetailModel: NSObject {
    
    var store_id = ""
    var store_name = ""
    var store_email = ""
    var store_phone = ""
    var store_address = ""
    var store_description = ""
    var store_latitude = ""
    var store_longitude = ""
    var store_distance = ""
    var deal_count = 0
    var coupon_count = 0
    var store_image = ""

    
     func parse(_ details: JSON) {
        
        store_id = details["store_id"].string ?? ""
        store_name = details["store_name"].string ?? ""
        store_email = details["store_email"].string ?? ""
        store_address = details["store_address"].string ?? ""
        store_phone = details["store_phone"].string ?? ""
        store_description = details["store_description"].string ?? ""
        store_latitude = details["store_latitude"].string ?? ""
        store_longitude = details["store_longitude"].string ?? ""
        store_distance = details["store_distance"].string ?? ""
        deal_count = details["deal_count"].int ?? 0
        coupon_count = details["coupon_count"].int ?? 0
        store_image = details["store_image"].string ?? ""
       
        
        
    }
}
