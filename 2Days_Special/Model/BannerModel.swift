//
//  BannerModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 29/12/20.
//

import Foundation
import SwiftyJSON

class BannerModel: NSObject {
    
   
    var id = ""
    var url = ""
    
  
    
     func parse(_ details: JSON) {
        
        id = String(details["id"].int!) ?? "0"
        url = details["url"].string ?? ""

    }
}
