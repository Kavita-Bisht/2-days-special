//
//  ManageDealModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 26/12/20.
//

import Foundation
import SwiftyJSON

struct ManageDealModel {
    
    let deal_name : String
    let deal_type : String
    let availed_by : String
    
}
