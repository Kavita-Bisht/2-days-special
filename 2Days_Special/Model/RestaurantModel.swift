//
//  RestaurantModel.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 30/12/20.
//

import Foundation


struct RestaurantModel {
    let mainImage: String
    let name: String
    let area: String
    let noDeals: String
    let noCoupons: String
    
}
