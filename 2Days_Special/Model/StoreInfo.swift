//
//  StoreInfo.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 30/12/20.
//

import Foundation

import SwiftyJSON

class StoreInfo: NSObject {
    
    static var sharedInstance = StoreInfo()
    
    var auth_id = ""
    var store_id = ""
    var user_type = ""
    var store_email = ""
    var store_phone = ""
    var store_username = ""
    var store_name = ""
    var store_address = ""
    var store_description = ""
    var store_timezone = ""
    var store_gmt = ""
    var store_latitude = ""
    var store_longitude = ""
    var store_image = ""
    var store_owner = ""
    
    class func parse(_ details: JSON) {
        
        if let auth_id = details["auth_id"].string {
            StoreInfo.sharedInstance.auth_id = auth_id
            UserDefaults.standard.set(auth_id, forKey: "user_id")
        }
        
        if let store_id = details["store_id"].string {
            StoreInfo.sharedInstance.store_id = store_id
            UserDefaults.standard.set(store_id, forKey: "store_id")
        }
        
        if let username = details["store_username"].string {
            StoreInfo.sharedInstance.store_username = username
            UserDefaults.standard.set(username, forKey: "store_username")
        }
        
        if let name = details["store_details"]["store_name"].string {
            StoreInfo.sharedInstance.store_name = name
            UserDefaults.standard.set(name, forKey: "store_name")
        }
      
        
        if let email = details["store_details"]["store_email"].string {
            StoreInfo.sharedInstance.store_email = email
            UserDefaults.standard.set(email, forKey: "store_email")
        }
        
        if let phoneno = details["store_details"]["store_phone"].string {
            StoreInfo.sharedInstance.store_phone = phoneno
            UserDefaults.standard.set(phoneno, forKey: "store_phone")
        }
      
        if let address = details["store_details"]["store_address"].string {
            StoreInfo.sharedInstance.store_address = address
            UserDefaults.standard.set(address, forKey: "store_address")
        }
        if let description = details["store_details"]["store_username"].string {
            StoreInfo.sharedInstance.store_description = description
            UserDefaults.standard.set(description, forKey: "store_description")
        }
        if let timezone = details["store_details"]["store_timezone"].string {
            StoreInfo.sharedInstance.store_timezone = timezone
            UserDefaults.standard.set(timezone, forKey: "store_timezone")
        }
        if let gmt = details["store_details"]["store_gmt"].string {
            StoreInfo.sharedInstance.store_gmt = gmt
            UserDefaults.standard.set(gmt, forKey: "store_gmt")
        }
        
        if let latitude = details["store_details"]["store_latitude"].string {
            StoreInfo.sharedInstance.store_latitude = latitude
            UserDefaults.standard.set(latitude, forKey: "store_latitude")
        }
        
        if let longitude = details["store_details"]["store_longitude"].string {
            StoreInfo.sharedInstance.store_longitude = longitude
            UserDefaults.standard.set(longitude, forKey: "store_longitude")
        }
        
        if let image = details["store_details"]["store_image"].string {
            StoreInfo.sharedInstance.store_image = image
            UserDefaults.standard.set(image, forKey: "store_image")
        }
        
        if let owner = details["store_details"]["store_owner"].string {
            StoreInfo.sharedInstance.store_owner = owner
            UserDefaults.standard.set(owner, forKey: "store_owner")
        }
        
        UserDefaults.standard.synchronize()
    }
}
