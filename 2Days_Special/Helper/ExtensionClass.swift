//
//  ExtensionClass.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 17/12/20.
//

import Foundation
import UIKit
import AVKit
@available(iOS 11.0, *)

protocol ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable
}

enum ObjectSavableError: String, LocalizedError {
    case unableToEncode = "Unable to encode object into data"
    case noValue = "No data object found for the given key"
    case unableToDecode = "Unable to decode object into given type"
    
    var errorDescription: String? {
        rawValue
    }
}

extension UIViewController
{
    
    
    //    //MARK: show progress hud
    //    func showProgress() {
    //        let size = CGSize(width: 50, height:50)
    //        self.startAnimating(size, message:"Loading", messageFont: UIFont.systemFont(ofSize: 18.0), type: NVActivityIndicatorType.ballScaleRippleMultiple, color: UIColor.white, padding: 1, displayTimeThreshold: nil, minimumDisplayTime: nil)
    //    }
    //
    //    //MARK: hide progress hud
    //    func hideProgress() {
    //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
    //            self.stopAnimating() }
    //    }
    
    
    //MARK: ShowAlert...
    func ShowLogoutAlert(title: String, message: String, viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    //MARK: ShowAlertView...
    func ShowAlertView(controller: UIViewController, title: String, message: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        
            controller.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: Push Segue ...
    func performPushSeguefromController(identifier:String){
        let vc = storyboard?.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    
    //MARK: to clear all stored data on logout time ..
    func clearData(){
        LogoutFunction()
    }
    func LogoutFunction(){
        
    }
}

//
//extension UIView{
//   func shadowView(){
//          self.layer.shadowColor = UIColor.lightGray.cgColor
//          self.layer.shadowOpacity = 0.5
//          self.layer.shadowOffset = .zero
//          self.layer.shadowRadius = 5
//      }
//
//}

//MARK:- ===========Localization==========
extension String {
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func localized(loc:String) ->String {
        
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    func convertToAttributedFromHTML() -> NSAttributedString? {
        var attributedText: NSMutableAttributedString?
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue]
        if let data = data(using: .unicode, allowLossyConversion: true), let attrStr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) {
            // attrStr.addAttributes([NSAttributedString.Key.font : UIFont(name: "Barlow-Regular", size:15)!], range: NSMakeRange(0, attrStr.length))
            attributedText = attrStr
            
        }
        return attributedText
    }
    
    func requiredHeight(labelText:String) -> CGFloat {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let font = UIFont(name: "Helvetica", size: 16.0)
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width - 70, height: screenSize.height))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = labelText
        label.sizeToFit()
        return label.frame.height
        
    }
    
    func removeWhiteSpaceForImage() -> String
    {
        let whitespace = NSCharacterSet.whitespaces
        var imageURL = self
        let range = imageURL.rangeOfCharacter(from: whitespace)
        
        
        
        // range will be nil if no whitespace is found
        if let image = range {
            imageURL = imageURL.replacingOccurrences(of: " ", with: "%20")
          
        }
       
        
        return imageURL
    }
    
    func formatDate() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: self)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter1.dateFormat = "dd-MMM hh:mm a"
        dateFormatter1.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let dateInString = dateFormatter1.string(from: date)
        
        return dateInString
    }
    
    func dateCalculator() -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: self)
        print("date: \(date)")
        print(Date())
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.allowedUnits = [.day,.hour,.minute]
        dateComponentsFormatter.maximumUnitCount = 1
        dateComponentsFormatter.unitsStyle = .full
        let final = dateComponentsFormatter.string(from: date ?? Date() , to: Date())!
        
        //Date in String for print
        let dateFormatter1 = DateFormatter()
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter1.dateFormat = "dd-MMM"
        dateFormatter1.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let dateInString = dateFormatter1.string(from: date ?? Date())
        
        let dateFormatter3 = DateFormatter()
        dateFormatter3.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter3.dateFormat = "hh:mm a"
        dateFormatter3.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let hrInString = dateFormatter3.string(from: date ?? Date())
        
        //Time in String for print
        let dateFormatter2 = DateFormatter()
        dateFormatter2.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter2.dateFormat = "h:mm a"
        dateFormatter2.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let timeInString = dateFormatter2.string(from: date ?? Date())
        
        if(final.contains("minute"))
        {
            return final
            
        }else if(final.contains("hour"))
        {
            let token = final.components(separatedBy: " ")
            if(Int(token[0])! < 12)
            {
                return "\(final) ago"
            }else if(Int(token[0])! < 24  && Int(token[0])! > 12)
            {
                return "Today \(timeInString)"
            }else if(Int(token[0])! < 48  && Int(token[0])! > 24)
            {
                return "Yesterday \(timeInString)"
            }
        }else if(final.contains("day"))
        {
            let token = final.components(separatedBy: " ")
            if(Int(token[0])! >= 2  && Int(token[0])! <= 5)
            {
                return "\(final) ago"
            }else
            {
                return dateInString + " at " + hrInString
            }
        }
        
        return final
    }
    
    public func stringByAppendingPathComponent(_ path: String) -> String {
        let fileUrl = URL.init(fileURLWithPath: self)
        let filePath = fileUrl.appendingPathComponent(path).path
        return filePath
    }
    
}


extension Date {
    
    @nonobjc static var localFormatter: DateFormatter = {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateStyle = .medium
        dateStringFormatter.timeStyle = .medium
        return dateStringFormatter
    }()
    
    func localDateString() -> String
    {
        return Date.localFormatter.string(from: self)
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    
    
}

extension UIImageView {
    
    func makeRounded() {
        
        
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
        
    }
}


extension Bundle {
    
    static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
            return view
        }
        
        fatalError("Could not load view with type " + String(describing: type))
    }
}


extension UIView {
    
    /// Adds constraints to this `UIView` instances `superview` object to make sure this always has the same size as the superview.
    /// Please note that this has no effect if its `superview` is `nil` – add this `UIView` instance as a subview before calling this.
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
        self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 0).isActive = true
        self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: 0).isActive = true
        
    }
}

extension Notification.Name {
    public static let cancle = Notification.Name(rawValue: "cancle")
    public static let done = Notification.Name(rawValue: "done")
    public static let apply = Notification.Name(rawValue: "apply")
    public static let reset = Notification.Name(rawValue: "reset")
    public static let viewMore = Notification.Name(rawValue: "View More")
    public static let searchDone = Notification.Name(rawValue: "searchDone")
    public static let searchCancle = Notification.Name(rawValue: "searchCancle")
    public static let languageChange = Notification.Name(rawValue: "languageChange")
}

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}

extension Array {
    subscript(safe index: Index) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}


extension UserDefaults: ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            set(data, forKey: forKey)
        } catch {
            throw ObjectSavableError.unableToEncode
        }
    }
    
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable {
        guard let data = data(forKey: forKey) else { throw ObjectSavableError.noValue }
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        } catch {
            throw ObjectSavableError.unableToDecode
        }
    }
}





