//
//  AppHelper.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 17/12/20.
//

import Foundation
import UIKit
import Toast_Swift

class AppHelper: NSObject {
    var appLoaderView: AppLoaderView?
    static let sharedInstance = AppHelper()

    
    

    
    func displaySpinner() {
        if (appLoaderView != nil) {
            removeSpinner()
        }
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            guard let weakObj = weakSelf else { return }
            weakObj.appLoaderView = AppLoaderView(frame: UIScreen.main.bounds)
            APPDELEGATE.window?.addSubview(weakObj.appLoaderView!)
        }
    }
    
    
    
    func removeSpinner() {
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.appLoaderView?.removeFromSuperview()
            weakSelf?.appLoaderView = nil;
        }
    }
    
    class func shadowView(_ view: UIView){
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 5
    }

    
     


    class func isInterNetConnectionAvailable() -> Bool {

        if Reachability.isConnectedToNetwork() {
            return true
        }else {
            let alert: UIAlertController = UIAlertController(title: AppName, message: "Please check your internet connection and try again", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okAction)
            alert.preferredAction = okAction
           // alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                navVC.present(alert, animated: true, completion: nil)

            }
            
            return false
        }
    }
    

    
  
    
    // Check valid Email
     class func isValidEmail(testStr:String) -> Bool {
         let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
         
         let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
         return emailPred.evaluate(with: testStr)
     }

}
