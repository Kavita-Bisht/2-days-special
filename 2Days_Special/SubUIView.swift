//
//  SubUIView.swift
//  Practice1
//
//  Created by MT on 19/02/20.
//  Copyright © 2020 MT. All rights reserved.
//

import UIKit

class SubUIView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var subMainView: UIView!
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CustomAlertView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    func changeBounds()
    {
        backView.layer.cornerRadius = 7
        backView.clipsToBounds = true
        
    }
    @IBAction func cancelButton(_ sender: Any) {
        self.removeFromSuperview()
    }
}
