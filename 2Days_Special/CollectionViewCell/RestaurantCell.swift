//
//  RestaurantCollectionViewCell.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 14/12/20.
//

import UIKit

class RestaurantCell: UICollectionViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var dealLabel: UIButton!
    @IBOutlet weak var couponLabel: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 15
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        
    }
    
    public func configure(with model : StoreDetailModel){
        
        mainImage.load(url: URL(string: model.store_image)!)
        nameLabel.text = model.store_name
        areaLabel.text = model.store_address
        dealLabel.setTitle(String(model.deal_count) + " Deals", for: .normal)
        couponLabel.setTitle(String(model.coupon_count) + " Coupons", for: .normal)
        
    }
    
    
    
    
}
