

import UIKit

class SecondCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainViewCell: UIView!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var btnShowProductDesc: UIButton!
    @IBOutlet weak var lblProductprice: UILabel!
    @IBOutlet weak var imageViewProduct: UIImageView!
   
    @IBOutlet weak var btnClaim: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainViewCell.layer.cornerRadius = 7
        mainViewCell.clipsToBounds = true
        btnClaim.layer.cornerRadius = 3
        btnClaim.clipsToBounds = true
        mainViewCell.layer.shadowColor = UIColor.black.cgColor
        mainViewCell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        mainViewCell.layer.shadowOpacity = 0.5
        mainViewCell.layer.shadowRadius = 4.0
        // Initialization code
    }
   
    
    @IBAction func btnClaimAction(_ sender: Any) {
      
    }
}
