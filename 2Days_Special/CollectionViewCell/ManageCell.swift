//
//  ManageCell.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 26/12/20.
//

import UIKit

protocol ActionClickCallback {
    func actionClick(_ dealId: String)
}

class ManageCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellContainer: UIView!
    
    var action:ActionClickCallback? = nil
    
    var deal : DealModel? = nil
    
    @IBOutlet weak var actionView: UIView!
    
    
    @IBOutlet weak var mName: UILabel!
    
    @IBOutlet weak var mExpired: DesignableButton!
    
    @IBOutlet weak var mAvailedBy: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellContainer.layer.backgroundColor = UIColor.white.cgColor
        cellContainer.layer.masksToBounds = false
        cellContainer.layer.shadowColor = UIColor.lightGray.cgColor
        cellContainer.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        cellContainer.layer.shadowOpacity = 0.8
        cellContainer.layer.shadowRadius = 0.0
        cellContainer.layer.shadowOpacity = 0.6
        
        let actionGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(actionTap(tapGestureRecognizer:)))
        actionView.isUserInteractionEnabled = true
        actionView.addGestureRecognizer(actionGestureRecognizer)
        
    }
    
    public func config(deal : DealModel, action : ActionClickCallback){
        self.action = action
        mName.text = deal.deal_name
        if deal.deal_type == "1"{
            mExpired.setTitle("Ongoing", for: .normal)
            mExpired.backgroundColor = UIColor(red: 218, green: 255, blue: 242, alpha: 1)
            mExpired.setTitleColor(UIColor.green, for: .normal)
        }
        else if deal.deal_type == "2"{
            mExpired.setTitle("Expired", for: .normal)
            mExpired.backgroundColor = UIColor(red: 255, green: 237, blue: 234, alpha: 1)
            mExpired.setTitleColor(UIColor.red, for: .normal)
        }
       
        mAvailedBy.text = deal.person_avail
        
        
    }
    
    @objc func actionTap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("Action clicked")
        action?.actionClick(deal!.deal_id)
    }
    
    
    
    @IBAction func actionTap(_ sender: Any) {
        print("Action clicked")
        action?.actionClick(deal!.deal_id)
        
    }
    @IBAction func actionDealTap(_ sender: Any) {
      //  print("Action clicked")
      //  action?.actionClick(deal!.deal_id)
    }
}
