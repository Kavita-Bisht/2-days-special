//
//  FoodCategoryCell.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 14/12/20.
//

import UIKit 

class FoodCategoryCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.layer.cornerRadius = 19
    }
    
    override func layoutSubviews() {
        // cell rounded section
        self.layer.cornerRadius = self.layer.frame.height / 2
//               self.layer.borderWidth = 5.0
//               self.layer.borderColor = UIColor.clear.cgColor
//               self.layer.masksToBounds = true
               
               // cell shadow section
//               self.contentView.layer.cornerRadius = 5
//               self.contentView.layer.borderWidth = 5.0
//               self.contentView.layer.borderColor = UIColor.clear.cgColor
//               self.contentView.layer.masksToBounds = true
//               self.layer.shadowColor = UIColor.white.cgColor
//               self.layer.shadowOffset = CGSize(width: 0, height: 0.0)
//               self.layer.shadowRadius = 6.0
//               self.layer.shadowOpacity = 0.6
//               self.layer.cornerRadius = 15.0
//               self.layer.masksToBounds = false
//               self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
    public func configure(with model : CategoryModel){
        
        title.text = model.name
        
    }

}
