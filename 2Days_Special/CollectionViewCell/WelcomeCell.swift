//
//  WelcomeCell.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 16/12/20.
//

import UIKit

class WelcomeCell: UICollectionViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var descLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        descLabel.frame.size.width = 100
        descLabel.lineBreakMode = .byWordWrapping
        descLabel.numberOfLines = 0
    }
    
    public func configure(with mainImage : String, desc : String){
        
        self.mainImage.image = UIImage(named: mainImage)
        self.descLabel.text = desc
        
    }

}
