//
//  BannerCollectionViewCell.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 30/12/20.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mBannerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mBannerImage.layer.shadowColor = UIColor.lightGray.cgColor
        mBannerImage.layer.shadowOffset = CGSize(width:0, height: 10)
        mBannerImage.layer.shadowRadius = 6.0
        mBannerImage.layer.shadowOpacity = 0.4
        mBannerImage.layer.masksToBounds = false
    }

    public func configure(with model : BannerModel){
        
        mBannerImage.load(url: URL(string: model.url)!)
       
        
    }
    
}
