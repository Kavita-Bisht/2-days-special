//
//  EndPoint.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 17/12/20.
//

import Foundation
import UIKit
import SystemConfiguration

let AppName = "2 days special"
let kErrorMsg = "Something went wrong"
let kLoginError = "Please login first before accessing further process"

let userID:String = UserDefaults.standard.string(forKey: "user_id") ?? ""

let ALERT_MESSSAGE = "Make sure, Your internet connection."
let Loader_ICON_COLOR = #colorLiteral(red: 0.568627451, green: 0.05490196078, blue: 0.05098039216, alpha: 1)
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let BASEURL = "http://52.37.151.212/"

let USER_SIGN_IN =  BASEURL + "user-login"
let USER_SIGN_UP =  BASEURL + "user-signup"
let BUSINESS_SIGN_UP = BASEURL + "business-signup"
let STORE_DETAIL = BASEURL + "store-detail"
let UPDATE_PASSWORD =  BASEURL + "update-password"
let UPDATE_PROFILE = BASEURL + "update-profile"
let STORE_DASHBOARD = BASEURL + "store-dashboard"
let DELETE_COUPON_OR_DEAL = BASEURL +  "deal-coupon"
let ADD_COUPON_OR_DEAL = BASEURL + "deal-coupon"
let STORE_NEARBY = BASEURL + "nearby-stores"
let ABOUT_US = BASEURL + "about-term"
let TERM = BASEURL + "about-term"
let STORE_CATEGORY = BASEURL + "seller"

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

