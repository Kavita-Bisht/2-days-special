
//
//  APIManager.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 17/12/20.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift


/*
 This Class is used for creating link between Backend Api and Our View Controller
 */
class APIManager : NSObject{
    

    class func performDeleteRequest(urlString: String, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        let apiToken = UserDefaults.standard.string(forKey: "accessToken") ?? ""
        print(urlString);
        let osVersion = UIDevice.current.systemVersion
        let langaugeID = UserDefaults.standard.string(forKey: "LanguageID") ?? "1"
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "secret_key": "fsjdos2dk58SHJkdjcmlsowuh",
            "secret_string": "hawilti#String@123",
            "hash": "d65bf389e9b2696d86ee9d1833f345c90c1d2eeccf2f4a8738a7d8a2dfefcc90",
            "token": apiToken,
            "language" :  langaugeID
        ]
        
        AF.request(urlString, method : .delete, parameters : nil, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            
            let statusCode = dataResponse.response?.statusCode
            if let data = dataResponse.data{
                let responseJSON = JSON(dataResponse.data!)
                print(data)
                
            }
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    completionBlock(responseJSON, nil)
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
    }
    
    class func performGETRequest(urlString: String, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        let apiToken = UserDefaults.standard.string(forKey: "accessToken") ?? ""
        print(urlString);
        let osVersion = UIDevice.current.systemVersion
        let langaugeID = UserDefaults.standard.string(forKey: "LanguageID") ?? "1"
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "secret_key": "fsjdos2dk58SHJkdjcmlsowuh",
            "secret_string": "hawilti#String@123",
            "hash": "d65bf389e9b2696d86ee9d1833f345c90c1d2eeccf2f4a8738a7d8a2dfefcc90",
            "token": apiToken,
            "language" :  langaugeID
            
        ]
        
        AF.request(urlString, method : .get, parameters : nil, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            
            let statusCode = dataResponse.response?.statusCode
            if let data = dataResponse.data{
                let responseJSON = JSON(dataResponse.data!)
                print(data)
                
            }
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    completionBlock(responseJSON, nil)
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
    }
    
    class func performPOSTRequest(urlString: String, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        let apiToken = "f8551207a3292ba9dedc79b991b2bc6cff33aa2d"//UserDefaults.standard.string(forKey: "accessToken")
        let timezone = TimeZone.current
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "accessToken": apiToken,
            "timeZone" : timezone.description
        ]
        
        print(params)
        
        AF.request(urlString, method : .post, parameters : params, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            let statusCode = dataResponse.response?.statusCode
            if let data = dataResponse.data{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                
            }
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    completionBlock(responseJSON, nil)
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
        
    }
    
    class func performPUTRequest(urlString: String, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        let apiToken = UserDefaults.standard.string(forKey: "accessToken")
        let langaugeID = UserDefaults.standard.string(forKey: "LanguageID") ?? "1"
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "secret_key": "fsjdos2dk58SHJkdjcmlsowuh",
            "secret_string": "hawilti#String@123",
            "hash": "d65bf389e9b2696d86ee9d1833f345c90c1d2eeccf2f4a8738a7d8a2dfefcc90",
            "token": apiToken ?? "",
            "language" : langaugeID
        ]
        print(params)
        AF.request(urlString, method : .put, parameters : params, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            let statusCode = dataResponse.response?.statusCode
            if let data = dataResponse.data{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                
            }
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    completionBlock(responseJSON, nil)
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
        
    }
    
    class  func preforImageWithPUTRequest(urlString: String, andParameters params: Parameters?,andImage image:Data, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void){
        let langaugeID = UserDefaults.standard.string(forKey: "LanguageID")
        
        let apiToken =  "f8551207a3292ba9dedc79b991b2bc6cff33aa2d" //UserDefaults.standard.string(forKey: "accessToken")
        
       
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "accessToken": apiToken,
            "timeZone" : timezone.description
        ]
        
        AF.upload(multipartFormData: { (MultipartFormData) in
            
            
          MultipartFormData.append(image, withName: "image", fileName: "file.jpg", mimeType: "image/jpg")
            for p in params! {
                MultipartFormData.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            //MultipartFormData.append(image.jpegData(compressionQuality: 0.4)!, withName: "profile_image", fileName: "file.jpg", mimeType: "image/jpg")
           // MultipartFormData.append(image.pngData()!, withName: "profile_image", fileName: "file.jpg", mimeType: "image/jpg")
            
           
        }, to: urlString,  method: .post, headers: headers).responseData { dataResponse in
            let statusCode = dataResponse.response?.statusCode
            if let data = dataResponse.data{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                
            }
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{
                    completionBlock(responseJSON, nil)
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
        
        
    }
    
}


