//
//  LoginSubView.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 23/12/20.
//

import UIKit


class LoginSubView: UIView {

    
    @IBOutlet weak var backView: UIView!
   
    
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("LoginSubView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    
    
  
    @IBAction func okTap(_ sender: Any) {
      
        
        // Navigate to User Home
        let userHomeVC = self.parentViewController?.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let navigationVC = UINavigationController(rootViewController: userHomeVC)
        navigationVC.isNavigationBarHidden = true
        
        window!.rootViewController = navigationVC
        window!.makeKeyAndVisible()
        
        self.removeFromSuperview()
        
    }
    
    
    func changeBounds()
    {
        backView.layer.cornerRadius = 7
        backView.clipsToBounds = true
        
    }
   
}
