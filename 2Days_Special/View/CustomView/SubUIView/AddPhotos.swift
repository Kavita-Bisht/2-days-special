//
//  AddPhotos.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 30/12/20.
//

import UIKit

//protocol AddPhotosProtocol {
//    func onPhotoClick()
//    func onCameraClick()
//    func onPreviousPhotosClick()
//}

class AddPhotos: UIView{
    
    var controller : CafeDetailViewController =  CafeDetailViewController()
    

    @IBOutlet weak var backView: UIView!
       
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    @IBAction func clickPictureTap(_ sender: Any) {
        self.removeFromSuperview()
        
        controller.onCameraClick()
        
    }
    
    @IBAction func photoGalleryTap(_ sender: Any) {
        self.removeFromSuperview()
        controller.onPhotoClick()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("AddPhotos", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    
    @IBAction func previousTap(_ sender: Any) {
        
        self.removeFromSuperview()
        
        self.controller.onPreviousPhotosClick()
    }
    
    @IBAction func cancelTap(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    
    
    func changeBounds(controller : UIViewController)
    
    {
        
        self.controller = controller as! CafeDetailViewController
        backView.layer.cornerRadius = 7
        backView.clipsToBounds = true
        
    }
   

}
