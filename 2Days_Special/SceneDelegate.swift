//
//  SceneDelegate.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 10/12/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
       configureInitialViewController()
        
        
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

extension SceneDelegate {
    func configureInitialViewController(){
        // CHECKING FOR WELCOME SCREEN
        let welcomeScreen :String = UserDefaults.standard.string(forKey: "welcome") ?? "0"
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        
        if welcomeScreen == "1"
        {
            // Navigate to Login
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            let navigationVC = UINavigationController(rootViewController: loginVC)
            navigationVC.isNavigationBarHidden = true
            
            window!.rootViewController = navigationVC
            window!.makeKeyAndVisible()
            
            
        }
        
        // CHECKING FOR LOGIN SCREEN
        let loginScreen :String = UserDefaults.standard.string(forKey: "is_login") ?? "0"
        
        if loginScreen == "1"
        {
            
            let account_type :String = UserDefaults.standard.string(forKey: "user_type") ?? "user"
            
            if account_type == "user" {
            
            // Navigate to User Home
            let userHomeVC = storyboard.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
            
            let navigationVC = UINavigationController(rootViewController: userHomeVC)
            navigationVC.isNavigationBarHidden = true
            
            window!.rootViewController = navigationVC
            window!.makeKeyAndVisible()
            }
            else if account_type == "business" {
                // Navigate to User Home
                let businessHomeVC = storyboard.instantiateViewController(withIdentifier: "BusinessHomeViewController") as! BusinessHomeViewController
                
                let navigationVC = UINavigationController(rootViewController: businessHomeVC)
                navigationVC.isNavigationBarHidden = true
                
                window!.rootViewController = navigationVC
                window!.makeKeyAndVisible()
                }
            
            
        }
    }
}
