//
//  ViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 10/12/20.
//

import UIKit
import SwiftyJSON
import Alamofire


class LoginViewController: UIViewController {
    
    // login view outlet
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var mEmailOrPhone: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    @IBOutlet weak var mLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupUI()
        
        
    }
    
    func setupUI(){
        
        // viewHolder cornerRadius & Shadow
        viewHolder.layer.cornerRadius = 45
        viewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        viewHolder.layer.shadowOffset = CGSize(width: 1, height: -6)
        viewHolder.layer.shadowOpacity = 0.3
        
        // add botoom border to text field
        mEmailOrPhone.setBottomBorder()
        mPassword.setBottomBorder()
        
        // border radius & shadow to login button
        mLoginButton.layer.cornerRadius = 19
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.mLoginButton.frame.size.width + shadowSize,
                                                   height: self.mLoginButton.frame.size.height + shadowSize))
        self.mLoginButton.layer.masksToBounds = false
        self.mLoginButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mLoginButton.layer.shadowColor = UIColor(red: 208, green: 0, blue: 0, alpha: 1).cgColor
        self.mLoginButton.layer.shadowOpacity = 0.2
        self.mLoginButton.layer.shadowPath = shadowPath.cgPath
        self.mLoginButton.layer.shadowRadius = 19
        
        
       
        
    }
    
    
    @IBAction func loginButtonTap(_ sender: Any) {
        
        // validation
        if mEmailOrPhone.text?.count == 0
        {
            mEmailOrPhone.setShadowColor(UIColor.red)
        }else
        {
            mEmailOrPhone.setShadowColor(UIColor.lightGray)
        }
        
        // if phone no is used
        if(mEmailOrPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil){
                
        
                if mEmailOrPhone.text!.count != 10
                {
                    mEmailOrPhone.setShadowColor(UIColor.red)
                }else
                {
                    mEmailOrPhone.setShadowColor(UIColor.lightGray)
                }
                
            
        }
        else{
            // for email id
            if mEmailOrPhone.text?.count == 0 || (mEmailOrPhone == nil) || AppHelper.isValidEmail(testStr: mEmailOrPhone.text!) == false
            {
                mEmailOrPhone.setShadowColor(UIColor.red)
            }else
            {
                mEmailOrPhone.setShadowColor(UIColor.lightGray)
            }
        }
        
        
        if(mPassword.text?.count == 0 || !(mPassword.text!.count >= 6 && mPassword.text!.count <= 15) ){
            mPassword.setShadowColor(UIColor.red)
        }else{
            mPassword.setShadowColor(UIColor.lightGray)
        }
        
        
        
        
        
        
        if(mEmailOrPhone.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill email or phone number")
            return
        }
        else if(mPassword.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill password")
            return
        }
        else if((mEmailOrPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil) )
        {
            if( mEmailOrPhone.text?.count != 10) {
            ShowAlertView(controller: self, title: AppName, message: "Phone no is incorrect")
                return
            }
            
        }
        
        if(mEmailOrPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil && AppHelper.isValidEmail(testStr: mEmailOrPhone.text!) == false){
                ShowAlertView(controller: self, title: AppName, message: "Email is incorrect")
            return
            }
      
        else if(!(mPassword.text!.count >= 6 && mPassword.text!.count <= 15))
        {
            ShowAlertView(controller: self, title: AppName, message: "Password must be between 6 to 15 characters")
            return
        }
        
        // calling api
        let params = ["email_or_phone": mEmailOrPhone.text!,
                      "password": mPassword.text!,
                      "option": "login"]
        
        callLoginAPI(params: params, isNeedToShowLoader: true )
    
    }
    
    func isStringContainsOnlyNumbers(string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func callLoginAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = USER_SIGN_IN
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                        var user_type = result!["response"]["user_type"].string
                        
                        
                        
                        UserDefaults.standard.setValue(user_type, forKey: "user_type")

                        
                       if( user_type == "user")
                       {
                        
                        UserModel.parse( result!["response"])
                        
                       }else if user_type == "business"{
                        StoreInfo.parse(result!["response"])
                       }
                    
                        
                        // Navigate to UserHome
                        
                        
                        UserDefaults.standard.setValue("1", forKey: "is_login")
                      
                        UserDefaults.standard.synchronize()
                        
                        if user_type == "user"{
                        
                        let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
                        
                        self.navigationController!.pushViewController(homeVC, animated: true)
                        }
                        else if user_type == "business"{
                            
                            let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "BusinessHomeViewController") as! BusinessHomeViewController
                            
                            self.navigationController!.pushViewController(homeVC, animated: true)
                        }
                        
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    
    
}




extension UITextField {
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.6
    }

}


extension UITextView {
    
    @objc func setBottomBorder() {
        //self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.6
    }
}

//extension UIView {
//
//        func setBottomBorder() {
//            //self.borderStyle = .none
//            self.layer.backgroundColor = UIColor.white.cgColor
//            self.layer.masksToBounds = false
//            self.layer.shadowColor = UIColor.lightGray.cgColor
//            self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
//            self.layer.shadowOpacity = 0.8
//            self.layer.shadowRadius = 0.0
//            self.layer.shadowOpacity = 0.6
//        }
//}
