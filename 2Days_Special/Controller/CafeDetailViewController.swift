//
//  CafeDetailViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 30/12/20.
//

import UIKit

class CafeDetailViewController: UIViewController {
    
    @IBOutlet weak var mStoreName: UILabel!
    
    @IBOutlet weak var mStoreAddress: UILabel!
    
    @IBOutlet weak var mStoreDescription: UILabel!
    
    @IBOutlet weak var mHeaderImage: UIImageView!
    
    func onPhotoClick() {
        
        
        ImagePickerManager2().pickImage(self, option: "gallery"){ image in
            //here is the image
            
                print("ello")
           
        }
    }
    
    func onCameraClick() {
        
        ImagePickerManager2().pickImage(self, option: "camera"){ image in
            //here is the image
           
        }
        
    }
    
    func onPreviousPhotosClick() {
        
    }
    
    @IBAction func backTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mHeaderImage.load(url: URL(string: UserDefaults.standard.string(forKey: "store_image")!)!)
        
        mStoreName.text = UserDefaults.standard.string(forKey: "store_name")
        mStoreAddress.text = UserDefaults.standard.string(forKey: "store_address")
        mStoreDescription.text = UserDefaults.standard.string(forKey: "store_description")
    }
    
    @IBAction func addPhotosTap(_ sender: Any) {
        
        _ = UIApplication.shared.delegate as! AppDelegate
        
        //  _ = self.view.window?.windowScene?.delegate
        let customView = AddPhotos(frame: self.view.frame)
        self.view.window?.addSubview(customView)
        customView.changeBounds(controller : self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
