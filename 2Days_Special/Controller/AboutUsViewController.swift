//
//  AboutUsViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 29/12/20.
//

import UIKit
import Alamofire
import WebKit
import SwiftyJSON

class AboutUsViewController: UIViewController {
    
    
    @IBOutlet weak var TopBar: UIView!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var mWebView: WKWebView!
    
    
    @IBOutlet weak var mTitle: UILabel!
    
    var ABOUT_OR_CONTACT_OR_TERM = "about"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        TopBar.clipsToBounds = true
        TopBar.layer.cornerRadius = 33
        TopBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        
        // calling api
        
        if ABOUT_OR_CONTACT_OR_TERM == "about" {
            
            mTitle.text = "About Us"
            
            let params = ["option":"about_us"]
            
            callAboutUsAPI(url: ABOUT_US , params: params, isNeedToShowLoader: true )
            
        }
        else if ABOUT_OR_CONTACT_OR_TERM == "contact" {
            
            mTitle.text = "CONTACT_US"
            
            let params = ["option":"about_us"]
            
            callAboutUsAPI(url: ABOUT_US, params: params, isNeedToShowLoader: true )
        }
        else if ABOUT_OR_CONTACT_OR_TERM == "term" {
            
            mTitle.text = "Term and Condition"
            
            let params = ["option":"term_condition"]
            
            callAboutUsAPI(url: TERM, params: params, isNeedToShowLoader: true )
        }
        
        
        
    }
    
    func callAboutUsAPI(url: String, params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = url
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                        self.mWebView.loadHTMLString(result!["response"]["html"].string!, baseURL: nil)
                        
                        
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    
    @IBAction func backTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
