//
//  AddDealsViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 22/12/20.
//

import UIKit

import Alamofire

class AddDealsViewController: UIViewController {
    
    var DEAL_OR_COUPON : Int = 1  // 1 - deal 2 - coupon
    
    var ADD_OR_UPDATE_OPTION : String = "create"   // create or edit
    
    @IBOutlet weak var topBar: UIView!
    
    
    @IBOutlet weak var mName: UITextField!
    
    @IBOutlet weak var mDescription: UITextView!
    
    @IBOutlet weak var mStartDate: UITextField!
    
    @IBOutlet weak var mEndDate: UITextField!
    
    @IBOutlet weak var mImagePickerView: UIImageView!
    
    @IBOutlet weak var mIsSpecialCoupon: Checkbox!
    
    
    @IBOutlet weak var mTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        
        // TopBar View UI Setup
        topBar.clipsToBounds = true
        topBar.layer.cornerRadius = 33
        topBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        
        mName.setBottomBorder()
        mDescription.setBottomBorder()
        //mStartDate.setBottomBorder()
        
        let imagePickerGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imagePickerTap(tapGestureRecognizer:)))
        mImagePickerView.isUserInteractionEnabled = true
        mImagePickerView.addGestureRecognizer(imagePickerGestureRecognizer)
        
        if DEAL_OR_COUPON == 1{
            mTitle.text = "Add Deals"
        }
        else if DEAL_OR_COUPON == 2{
            mTitle.text = "Add Coupns"
        }
        else if DEAL_OR_COUPON == 3 {
            mTitle.text = "Manage Special Offers"
            mIsSpecialCoupon.isUserInteractionEnabled = false; // or YES, as you desire.

        }
        
        
    }
    
    @objc func imagePickerTap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.mImagePickerView.image =  image
    }
    }
    
    
    @IBAction func saveTap(_ sender: Any) {
        
        
        // validation
        
        
        // calling api
        
        if ADD_OR_UPDATE_OPTION == "create"
        {
            let params = ["option" : "create",
                          "deal_or_coupon" : String(DEAL_OR_COUPON),
                          "store_id": UserDefaults.standard.string(forKey: "store_id"),
                          "name": mName.text!,
                          "description": mDescription.text!,
                          "start_date" : mStartDate.text! ,
                          "end_date": mEndDate.text!,
                          "is_special": mIsSpecialCoupon.isChecked ? "1" : "0"
            ]
            
            callAddOrUpdateDealAPI(params: params, isNeedToShowLoader: true)
        }
        else if ADD_OR_UPDATE_OPTION == "edit"{
            
            let params = ["option" : "edit",
                          "deal_or_coupon" : String(DEAL_OR_COUPON),
                          "store_id": UserDefaults.standard.string(forKey: "store_id"),
                          "name": mName.text!,
                          "description": mDescription.text!,
                          "start_date" : mStartDate.text! ,
                          "end_date": mEndDate.text!,
                          "is_special": mIsSpecialCoupon.isChecked ? "1" : "0"
            ]
            
            callAddOrUpdateDealAPI(params: params, isNeedToShowLoader: true)
        }
        
    }
    
    
    func callAddOrUpdateDealAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = ADD_COUPON_OR_DEAL
        
        APIManager.preforImageWithPUTRequest(urlString: urlString, andParameters: params, andImage: self.mImagePickerView.image!.jpegData(compressionQuality: 0.01)!) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                    // update UI
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
                }
                
            }
            
        }
        
        
//        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
//            
//            AppHelper.sharedInstance.removeSpinner()
//            
//            if result != nil{
//                
//                let statusCode = result!["status"].stringValue
//                
//                let message = result!["message"].string ?? kErrorMsg
//                
//                print(message)
//                
//                if statusCode == "200" {
//                    
//                    self.ShowAlertView(controller: self,title: AppName, message: message)
//                    
//                    // update UI
//                }
//                else{
//                    self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
//                }
//                
//            }
//            
//        }
    }
    
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
