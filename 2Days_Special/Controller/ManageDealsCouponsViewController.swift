//
//  WelcomeViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 16/12/20.
//

import UIKit
import Alamofire

class ManageDealsCouponsViewController: UIViewController, ActionClickCallback {
    
    
    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var manageDealCouponCollectionView: UICollectionView!
    
    
    @IBOutlet weak var navTitle: UILabel!
    
    var storeModel = StoreModel()
    
    var manageType : Int = 1
    
    // deal_coupon_option
    var alert = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    private func setupUI(){
        
        
        // TopBar View UI Setup
        topBar.clipsToBounds = true
        topBar.layer.cornerRadius = 33
        topBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        // title
        if manageType == 1 {
            
            navTitle.text = "Manage Deals"
            
        }else if manageType == 2{
            navTitle.text = "Manage Coupons"
        }
        else if manageType == 3 {
            navTitle.text = "Manage Special Offfers"
        }
        
        // Manage COLLECTION VIEW
        
        // 1. delegate
        manageDealCouponCollectionView.dataSource = self
        manageDealCouponCollectionView.delegate = self
        
        // 2. cell
        manageDealCouponCollectionView.register(UINib.init(nibName: "ManageCell", bundle: nil), forCellWithReuseIdentifier: "ManageCell")
        
        // 3. data
        
        
        // additional configuration
        manageDealCouponCollectionView.showsHorizontalScrollIndicator = false
        
        
        // action alert
        let editAction = UIAlertAction(title: "Edit", style: .default){
            UIAlertAction in
            
        }
        let deleteAction = UIAlertAction(title: "Delete", style: .default){
            UIAlertAction in
            
        }

        // Add the actions
       
        alert.addAction(editAction)
        alert.addAction(deleteAction)
     
        alert.popoverPresentationController?.sourceView = self.view
        
        
        // calling api
        let params = ["store_id": UserDefaults.standard.string(forKey: "store_id"),
                      "option":  "store-detail"]
        
        callManageAPI(params: params, isNeedToShowLoader: true )
       
       
    }
    
    func callManageAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_DETAIL
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                      
                        self.storeModel.parse(result!["response"])
                       
                    
                        
                        self.manageDealCouponCollectionView.reloadData()
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                
                }
                
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    
    
    
    func callDeleteCouponOrDealAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = DELETE_COUPON_OR_DEAL
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                   
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                   // update UI
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        
        }

    }
}
    
    
    @IBAction func backTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    @IBAction func addTap(_ sender: Any) {
        
        let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "AddDealsViewController") as! AddDealsViewController

        
            homeVC.DEAL_OR_COUPON = manageType
            homeVC.ADD_OR_UPDATE_OPTION = "create"
        
        
        self.navigationController!.pushViewController(homeVC, animated: true)
        
    }
  
    
    func actionClick(_ dealId: String) {
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// Welcome Collection View Adapter
extension ManageDealsCouponsViewController : UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if manageType == 1 {
            
            return storeModel.deals.count
            
        }else if manageType == 2{
            return storeModel.coupons.count
        }
        else if manageType == 3 {
            return 0
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = manageDealCouponCollectionView.dequeueReusableCell(withReuseIdentifier: "ManageCell", for: indexPath) as! ManageCell
        
        if manageType == 1 {
            
            cell.config(deal: storeModel.deals[indexPath.row], action: self)
            
        }else if manageType == 2{
            cell.config(deal: storeModel.coupons[indexPath.row], action: self)
        }
        else if manageType == 3 {
            
            
            cell.config(deal: storeModel.deals[indexPath.row], action: self)
        }
        
        
       
        
       
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 50)
    }

    
    
    
}
