//
//  buisnessRegisterViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 22/12/20.
//

import UIKit
import  Alamofire
import CoreLocation

class BusinessSignUp2ViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet weak var mNAme: UITextField!
    
    @IBOutlet weak var mCategory: DropDown!
    
    @IBOutlet weak var mSignUpButton: DesignableButton!
    
    @IBOutlet weak var mEmail: DesignableTextField!
    
    
    @IBOutlet weak var nPhone: DesignableTextField!
    
    @IBOutlet weak var viewHolder: DesignableButton!
    
    @IBOutlet weak var mLocation: UIButton!
    
    var locationManager = CLLocationManager()
    
    var address : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var city : String = ""
    var state : String = ""
    var zipcode : String = ""
    var country : String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mCategory.optionArray = ["Indian","Chinese","Italian","Sounth Indian"]
        
        setupUI()
        
        
        
    }
    func setupUI(){
        
        // viewHolder cornerRadius & Shadow
        viewHolder.layer.cornerRadius = 45
        viewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        viewHolder.layer.shadowOffset = CGSize(width: 1, height: -6)
        viewHolder.layer.shadowOpacity = 0.3
        
        //add botoom border to text field
        mNAme.setBorderBottom()
       // mCategory.setBorderBottom()
     //   mLocation.setBorderBottom()
       // mEmailID.setBorderBottom()
        //mPhoneNumber.setBorderBottom()
        //mbusinessOwner.setBorderBottom()
        
        //border radius & shadow to login button
        mSignUpButton.layer.cornerRadius = 19
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2.5,
                                                   y: -shadowSize / 2.5,
                                                   width: self.mSignUpButton.frame.size.width + shadowSize,
                                                   height: self.mSignUpButton.frame.size.height + shadowSize))
        self.mSignUpButton.layer.masksToBounds = false
        self.mSignUpButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mSignUpButton.layer.shadowColor = UIColor(red: 208, green: 0, blue: 0, alpha: 1).cgColor
        self.mSignUpButton.layer.shadowOpacity = 0.2
        self.mSignUpButton.layer.shadowPath = shadowPath.cgPath
        self.mSignUpButton.layer.shadowRadius = 19
        
    }
    
    
    
    func realtimeLocation() {
        
        // reuquest for enabling location permission
        locationManager.requestWhenInUseAuthorization()
        
        // callback delgegate to get updated location
        locationManager.delegate = self
        
        // asking for updating location
        locationManager.startUpdatingLocation()
        
        
    }
    
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   

}
