//
//  LocationPickerViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 18/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces


class LocationPickerViewController: UIViewController {

    @IBOutlet weak var doneTap: UIButton!
    
    @IBOutlet weak var mapContainer: UIView!
    
    var location: String = ""
    
    var resultsViewController: GMSAutocompleteResultsViewController?
      var searchController: UISearchController?
    var mapView:GMSMapView = GMSMapView()
    var marker = GMSMarker()
    
    var callback : ((String)->())?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: true)

        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
                // Create a GMSCameraPosition that tells the map to display the
                // coordinate -33.86,151.20 at zoom level 6.
                let camera = GMSCameraPosition.camera(withLatitude: 30.3165, longitude: 78.0322, zoom: 16)
                 mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
    
                self.mapContainer.addSubview(mapView)
        
        
        // GOOGLE MAPS SDK: COMPASS
                mapView.settings.compassButton = true

                // GOOGLE MAPS SDK: USER'S LOCATION
               mapView.isMyLocationEnabled = true
                mapView.settings.myLocationButton = true
              

                // Creates a marker in the center of the map.
                marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: 30.3165, longitude: 78.0322)
                //marker.title = "Dehradun"
                //marker.snippet = "My Location"
                marker.map = mapView
       
        resultsViewController = GMSAutocompleteResultsViewController()
            resultsViewController?.delegate = self

            searchController = UISearchController(searchResultsController: resultsViewController)
            searchController?.searchResultsUpdater = resultsViewController

            // Put the search bar in the navigation bar.
            searchController?.searchBar.sizeToFit()
            navigationItem.titleView = searchController?.searchBar

            // When UISearchController presents the results view, present it in
            // this view controller, not one further up the chain.
            definesPresentationContext = true

            // Prevent the navigation bar from being hidden when searching.
            searchController?.hidesNavigationBarDuringPresentation = false
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                self.navigationItem.leftBarButtonItem = newBackButton
            

           
    }

    @objc func back(sender: UIBarButtonItem) {
    // Perform your custom actions
    // ...
    // Go back to the previous ViewController
        callback?(location)
     _ = navigationController?.popViewController(animated: true)
}
    
    
    @IBAction func doneTap(_ sender: Any) {
        callback?(location)
    }
    
    
    // UpdteLocationCoordinate
//        func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
//            if marker == nil
//            {
//                marker = GMSMarker()
//                marker.position = coordinates
//                let image = UIImage(named:"destinationmarker")
//                marker.icon = image
//                marker.map = viewMap
//                marker.appearAnimation = kGMSMarkerAnimationPop
//            }
//            else
//            {
//                CATransaction.begin()
//                CATransaction.setAnimationDuration(1.0)
//                destinationMarker.position =  coordinates
//                CATransaction.commit()
//            }
//        }
    
    // Camera change Position this methods will call every time
//        func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
//            let destinationLocation = CLLocation()
//            if self.mapGesture == true
//            {
//                destinationLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
//                destinationCoordinate = destinationLocation.coordinate
//                updateLocationoordinates(destinationCoordinate)
//            }
//        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



// Handle the user's selection.
extension LocationPickerViewController: GMSAutocompleteResultsViewControllerDelegate {
  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                         didAutocompleteWith place: GMSPlace) {
    searchController?.isActive = false
    // Do something with the selected place.
    print("Place name: \(place.name)")
    print("Place address: \(place.formattedAddress)")
    
    location = place.formattedAddress ?? ""
    print("Place attributions: \(place.attributions)")
    print("Coordinates - Lat : \(place.coordinate.latitude)")
    print("Coordinates - Lng : \(place.coordinate.longitude)")
    
    let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16)
    mapView.animate(to: camera)
    // Creates a marker in the center of the map.
    marker = GMSMarker()
    marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
    //marker.title = "Dehradun"
    //marker.snippet = "My Location"
    marker.map = mapView
    
  }

  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                         didFailAutocompleteWithError error: Error){
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
}
