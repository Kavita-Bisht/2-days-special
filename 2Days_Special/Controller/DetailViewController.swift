//
//  DetailViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 15/12/20.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {
    
    var store_id = "0"

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var dealsCouponViewHolder: DesignableView!
    @IBOutlet weak var dealsViewHolder: UIView!
    
    @IBOutlet weak var mStoreName: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var mDescription: UILabel!
    
    var storeModel: StoreModel = StoreModel()
    
    @IBOutlet weak var couponStackView: UIStackView!
    
    @IBOutlet weak var dealStackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
        // calling API
        
        // calling api
        let params = ["store_id": store_id,
                      "option": "store-detail"]
        
        callStoreDetailAPI(params: params, isNeedToShowLoader: true )
       // descriptionOutlet.text = 
        
    }
    
    func setupUI(){
        
        // viewHolder cornerRadius & Shadow
        viewHolder.layer.cornerRadius = 45
        viewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        viewHolder.layer.shadowOffset = CGSize(width: 1, height: -6)
        viewHolder.layer.shadowOpacity = 0.3
        
        // deals coupon viewholder
        dealsCouponViewHolder.layer.cornerRadius = 10
        dealsCouponViewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        dealsCouponViewHolder.layer.shadowOffset = CGSize(width: 1, height: 1)
        dealsCouponViewHolder.layer.shadowOpacity = 0.5
        
    }
    
    func callStoreDetailAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_DETAIL
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                        self.storeModel = StoreModel()
                        self.storeModel.parse( result!["response"])
                        
                        // Navigate to UserHome
                        self.setupStore()
                        
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    func setupStore(){
        
        mStoreName.text = storeModel.store_name
        address.text = storeModel.store_address
        mDescription.text = storeModel.store_description == "" ? "No Description" : storeModel.store_description
        
        for d in storeModel.deals {
            let iv = UIImageView()
            iv.autoresizingMask = .flexibleHeight
            iv.frame.size.height = 280.0
            print(d.deal_image)
            iv.load(url: URL(string: d.image)!)
            
            dealStackView.addArrangedSubview(iv)

            
        }
        
        for d in storeModel.coupons {
            let iv = UIImageView()
            iv.autoresizingMask = .flexibleHeight
            iv.frame.size.height = 280.0
            print(d.deal_image)
            iv.load(url: URL(string: d.image)!)
            
            couponStackView.addArrangedSubview(iv)

            
        }
        
    }
    
    
    @IBAction func dealsCategoryClick(_ sender: Any) {
        dealsViewHolder.isHidden = dealsViewHolder.isHidden ? false : true
        //dealsViewHolder.frame.size.height = dealsViewHolder.frame.size.height == 0 ? 100 : 0
    }
    
    @IBAction func backTrapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
