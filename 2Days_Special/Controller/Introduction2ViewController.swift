//
//  Introduction2ViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 16/12/20.
//

import UIKit

class Introduction2ViewController: UIViewController {
    
    var pageIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func finishTap(_ sender: Any) {
        
        // NAVIGATE TO LOGIN SCREEN
        
       
        UserDefaults.standard.setValue("1", forKey: "welcome")
        UserDefaults.standard.synchronize()
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")

        self.navigationController?.pushViewController(vc, animated: true)
 
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
