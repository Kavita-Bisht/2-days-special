//
//  BusinessSideMenuTableViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 24/12/20.
//

import UIKit

class BusinessSideMenuTableViewController: UITableViewController {

    @IBOutlet weak var manageDeal: UIStackView!
    
    @IBOutlet weak var manageCoupons: UIStackView!
    
    @IBOutlet weak var manageSpecialOffers: UILabel!
    
    @IBOutlet weak var mStoreName: UILabel!
    
    @IBOutlet weak var mProfileImage: DesignableImage!
    
    
    @IBOutlet weak var mEditProfile: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        mStoreName.text = UserDefaults.standard.string(forKey: "store_name")
        
        
        mProfileImage.layer.cornerRadius = 50
        
        let manageDealtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(manageDealTap(tapGestureRecognizer:)))
       manageDeal.isUserInteractionEnabled = true
        manageDeal.addGestureRecognizer(manageDealtapGestureRecognizer)
        
        let manageCouponstapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(manageCouponsTap(tapGestureRecognizer:)))
        manageCoupons.isUserInteractionEnabled = true
        manageCoupons.addGestureRecognizer(manageCouponstapGestureRecognizer)
        
        let manageSpecialOffersGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(manageSpecialOffersTap(tapGestureRecognizer:)))
        manageSpecialOffers.isUserInteractionEnabled = true
        manageSpecialOffers.addGestureRecognizer(manageSpecialOffersGestureRecognizer)
        
        let editProfileGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(editProfileTap(tapGestureRecognizer:)))
        mEditProfile.isUserInteractionEnabled = true
        mEditProfile.addGestureRecognizer(editProfileGestureRecognizer)
        
    }
    
    @objc func editProfileTap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CafeDetailViewController") as! CafeDetailViewController;
       
    //    self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        // Your action
        
    }
    
    @objc func manageDealTap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ManageDealsCouponsViewController") as! ManageDealsCouponsViewController;
        vc.manageType = 1
    //    self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        // Your action
        
    }
    
    
    @objc func manageCouponsTap(tapGestureRecognizer: UITapGestureRecognizer) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ManageDealsCouponsViewController") as! ManageDealsCouponsViewController;
        vc.manageType = 2
    //    self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        // Your action
    }
    
    @objc func manageSpecialOffersTap(tapGestureRecognizer: UITapGestureRecognizer) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ManageDealsCouponsViewController") as! ManageDealsCouponsViewController;
        vc.manageType = 3
    //    self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        // Your action
    }
    
    @IBAction func signOutTap(_ sender: Any) {
        
        UserDefaults.standard.setValue("0", forKey: "is_login")
        UserDefaults.standard.synchronize()

        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
    //    self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        // Your action
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    

}
