//
//  ProfileViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 16/12/20.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var mname: UILabel!
    @IBOutlet weak var profileImageView: DesignableImage!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // corner radius profile image
        profileImageView.layer.cornerRadius = profileImageView.bounds.size.width / 2
      
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mname.text = UserDefaults.standard.string(forKey: "name")
    }
    
    @IBAction func backTap(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settingsTap(_ sender: Any) {
        
    }
    
    
    @IBAction func changePasswordTap(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
