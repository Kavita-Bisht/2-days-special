//
//  WelcomePageViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 16/12/20.
//

import UIKit


protocol OnClickDelegate {
    func onClick(_ any: Any)
}


class WelcomePageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    lazy var viewControllerList:[UIViewController] = {
        
        let sb = self.storyboard!
        
        let vc1 = sb.instantiateViewController(withIdentifier: "Introduction1ViewController") as! Introduction1ViewController
        vc1.pageIndex = 0
        vc1.delegate = self
        let vc2 = sb.instantiateViewController(withIdentifier: "Introduction2ViewController") as! Introduction2ViewController
        vc2.pageIndex = 1
        
        return [vc1, vc2]
    }()
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupPageViewController()
    }
    
    func setupPageViewController() {
        
        //1.Set its datasource and delegate methods
        self.dataSource = self
        self.delegate = self
        self.view.frame = .zero
        
        //2.Show view controller with initial page - page zero
        self.setViewControllers([viewControllerList[0]], direction: .forward, animated: false, completion: nil)
        
    }
    
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore
                                viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else { return nil }
        
        guard viewControllerList.count > previousIndex else { return nil }
        
        return viewControllerList[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = vcIndex + 1
        
        guard viewControllerList.count != nextIndex else { return nil }
        
        guard viewControllerList.count > nextIndex else { return nil }
        
        return viewControllerList[nextIndex]
        
    }
    
}

extension WelcomePageViewController : OnClickDelegate{
    
    func onClick(_ any: Any) {
        
        self.setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
        
    }
    
    
}

