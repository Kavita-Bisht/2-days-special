//
//  BusinessSugnUpViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 14/12/20.
//

import UIKit
import Alamofire
import CoreLocation

class BusinessSignUpViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mCategory: DropDown!
    @IBOutlet weak var mLocation: UITextField!
    @IBOutlet weak var mSignUpButton: DesignableButton!
    @IBOutlet weak var viewHolder: DesignableButton!
    @IBOutlet weak var mName: UITextField!

    @IBOutlet weak var mbusinessOwner: UITextField!
    @IBOutlet weak var mPhoneNumber: DesignableTextField!
    @IBOutlet weak var mEmailID: DesignableTextField!
    
    var locationManager = CLLocationManager()
    
    var address : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var city : String = ""
    var state : String = ""
    var zipcode : String = ""
    var country : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // REALTIME LOCATION
        //  getting location and timezone information
        //realtimeLocation()
        
        mCategory.optionArray = ["Indian","Chinese","Italian","South India"]
        
        setupUI()
        
        // calling api
        let params = [
                      "option": "categories"]
        
        print(params)
        
        callCategoryAPI(params: params as Parameters, isNeedToShowLoader: false)
    }
    
    func setupUI(){
        
        // viewHolder cornerRadius & Shadow
        viewHolder.layer.cornerRadius = 45
        viewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        viewHolder.layer.shadowOffset = CGSize(width: 1, height: -6)
        viewHolder.layer.shadowOpacity = 0.3
        
        //add botoom border to text field
        mName.setBorderBottom()
        mCategory.setBorderBottom()
        mLocation.setBorderBottom()
        mEmailID.setBorderBottom()
        mPhoneNumber.setBorderBottom()
        mbusinessOwner.setBorderBottom()
        
        //border radius & shadow to login button
        mSignUpButton.layer.cornerRadius = 19
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2.5,
                                                   y: -shadowSize / 2.5,
                                                   width: self.mSignUpButton.frame.size.width + shadowSize,
                                                   height: self.mSignUpButton.frame.size.height + shadowSize))
        self.mSignUpButton.layer.masksToBounds = false
        self.mSignUpButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mSignUpButton.layer.shadowColor = UIColor(red: 208, green: 0, blue: 0, alpha: 1).cgColor
        self.mSignUpButton.layer.shadowOpacity = 0.2
        self.mSignUpButton.layer.shadowPath = shadowPath.cgPath
        self.mSignUpButton.layer.shadowRadius = 19
        
    }
    
    func realtimeLocation() {
        
        // reuquest for enabling location permission
        locationManager.requestWhenInUseAuthorization()
        
        // callback delgegate to get updated location
        locationManager.delegate = self
        
        // asking for updating location
        locationManager.startUpdatingLocation()
        
        
    }
    
    // callback when location update
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            latitude = String(location.coordinate.latitude)
            longitude = String(location.coordinate.longitude)
            
            let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(location,
                                            completionHandler: { (placemarks, error) in
                                                if error == nil {
                                                    let firstLocation = placemarks?[0]
                                                    
                                                    print(firstLocation?.country)  // United States
                                                    self.country = firstLocation?.country ?? ""
                                                    
                                                    print(firstLocation?.timeZone) // America/Chicago
                                                    
                                                    self.address =  (firstLocation?.name ?? "")//SH1
                                                    self.city =   firstLocation?.locality ?? "" //Dehradun
                                                    self.state = firstLocation?.administrativeArea ?? "" //UT
                                                    self.zipcode =   firstLocation?.postalCode ??  "" //248001
                                                    
                                                    self.mLocation.text = self.address + ", " + self.city + ", " + self.state + ", " + self.zipcode
                                                    
                                                    
                                                }
                                                else {
                                                    
                                                    print("No location info")
                                                    
                                                }
                                            })
        }
        else{
            // No location was available.
            
            print("No location info")
            
        }
        
    }
    
    
    @IBAction func signUpUserButtonTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpBusinessButtonTap(_ sender: Any) {
        
        // validation
        if (mName.text?.count == 0)
        {
            mName.setShadowColor(UIColor.red)
        }else
        {
            mName.setShadowColor(UIColor.lightGray)
        }
        
        if (mbusinessOwner.text?.count == 0)
        {
            mbusinessOwner.setShadowColor(UIColor.red)
        }else
        {
            mbusinessOwner.setShadowColor(UIColor.lightGray)
        }
        
        if (mCategory.text?.count == 0)
        {
            mCategory.setShadowColor(UIColor.red)
        }else
        {
            mCategory.setShadowColor(UIColor.lightGray)
        }
        
        
        if (mEmailID.text?.count == 0 || AppHelper.isValidEmail(testStr: mEmailID.text!) == false)
        {
            mEmailID.setShadowColor(UIColor.red)
        }else
        {
            mEmailID.setShadowColor(UIColor.lightGray)
        }

        
        if(mPhoneNumber.text?.count == 0 || mPhoneNumber.text!.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil || mPhoneNumber.text?.count != 10 ){
            mPhoneNumber.setShadowColor(UIColor.red)
        }else{
            mPhoneNumber.setShadowColor(UIColor.lightGray)
        }
        
        if(mLocation.text?.count == 0){
            mPhoneNumber.setShadowColor(UIColor.red)
        }else{
            mPhoneNumber.setShadowColor(UIColor.lightGray)
        }
        
        
        if(mName.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill business name")
            return
        }
        else if(mbusinessOwner.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill business owner name")
            return
        }
        else if(mCategory.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please select business category")
            return
        }
        if(mEmailID.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill email")
            return
        }
       
        else if(mPhoneNumber.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill phone number")
            return
        }
      
        else if(mLocation.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please select location")
            return
        }
        
        else if(AppHelper.isValidEmail(testStr: mEmailID.text!) == false){
            ShowAlertView(controller: self, title: AppName, message: "Email is incorrect")
            return
        }
        else if(mPhoneNumber.text!.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil || mPhoneNumber.text?.count != 10)
        {
            ShowAlertView(controller: self, title: AppName, message: "Phone no is incorrect")
            return
        }
        
       
        
        // calling api
        let params = ["device_id" : UIDevice.current.identifierForVendor?.uuidString,
                      "device_type" : "2",
                      "device_token": "cj6zKwg7EhI:APA91bEImBmEJ6fq2pOHgPGKN5oaVEah0X2Xsu2ufz51BR6M5sbl5H_W-g4OmUSK8ol7WaPAtXYiC5tB-UKdJ8xjJekp0uHMtxgAlMuhyKxu1OK0eQL8AfmVUfsbLrZ305xQ5MjH8f5g",
                      "email": mEmailID.text!,
                      "business_name" : mName.text! ,
                      "business_owner_name" : mbusinessOwner.text!,
                      "category": mCategory.text!,
                      "phone": mPhoneNumber.text! ,
                      "address":address,
                      "latitude":latitude,
                      "longitude":longitude,
                      "city":city,
                      "state":state.count >= 3 ? state : "state" + ".",
                      "zipcode":zipcode,
                      "option": "signup"]
        
        print(params)
        
        callBusinessSignUpAPI(params: params as Parameters, isNeedToShowLoader: false)
    }
    
    func callBusinessSignUpAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = BUSINESS_SIGN_UP
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    
                    
                    
                    
                    _ = UIApplication.shared.delegate as! AppDelegate
                    
                    //  _ = self.view.window?.windowScene?.delegate
                    let customView = SubUIView(frame: self.view.frame)
                    self.view.window?.addSubview(customView)
                    customView.changeBounds()
                    
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    func callCategoryAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_CATEGORY
        
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { [self] (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].arrayValue != nil {
                        
                        print(result!["response"])
                        
                        mCategory.optionArray = []
                        
                      
                        
                        for i in result!["response"].arrayValue {
                            let category = CategoryModel()
                            category.parse(i)
                            
                            self.mCategory.optionArray.append(category.name)
                        }
                        
                        
                       
                     
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
  
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destination.
    // Pass the selected object to the new view controller.
       
       if segue.identifier == "locationPicker"{
               let vc = segue.destination as! LocationPickerViewController
               vc.callback = { result in
                   
                self.mLocation.text = result
                   
               }
        
        

    }
    
}
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension UITextField {
    
    func setBorderBottom() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.6
    }
    
    
    
    
    
    
}


