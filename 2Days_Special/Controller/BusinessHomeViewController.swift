//
//  UserViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 15/12/20.
//

import UIKit
import  Alamofire

class BusinessHomeViewController: UIViewController {
    
    @IBOutlet weak var totalDeals: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var imageCount: UILabel!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var totalCoupons: UILabel!
    
    // Images CardView Fields
    @IBOutlet weak var imagesViewContainer: DesignableView!
    
    
    // Cuisine CardView Fields
    @IBOutlet weak var cuisineViewContainer: DesignableView!
    
    // Total Coupons Availed Fields
    @IBOutlet weak var totalCouponsAvailedContainer: DesignableView!
    
    // Total Deals Availed Fields
    @IBOutlet weak var totalDealsAvailedContainer: DesignableView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupUI()
        
        
        let params = ["option": "store-dashboard",
                      "store_id": UserDefaults.standard.string(forKey: "store_id")
        ]
        
        
        callStoreDashboardAPI(params: params, isNeedToShowLoader: true )
    }
    
    // UI Customization
    private func setupUI(){
        
        
        // TopBar View UI Setup
        topBar.clipsToBounds = true
        topBar.layer.cornerRadius = 33
        topBar.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        //        self.topBar.layer.shadowOffset = CGSize(width: 0.0, height: 10)
        //         self.topBar.layer.shadowColor = UIColor.lightGray.cgColor
        //         self.topBar.layer.shadowOpacity = 0.4
        //        self.topBar.layer.shadowRadius = 33
        
        
        // Images Card View UI Setup
        imagesViewContainer.layer.cornerRadius = 15
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2.5,
                                                   y: -shadowSize / 2.5,
                                                   width: self.imagesViewContainer.frame.size.width + shadowSize,
                                                   height: self.imagesViewContainer.frame.size.height + shadowSize))
        self.imagesViewContainer.layer.masksToBounds = false
        self.imagesViewContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.imagesViewContainer.layer.shadowColor = UIColor.lightGray.cgColor
        self.imagesViewContainer.layer.shadowOpacity = 0.4
        self.imagesViewContainer.layer.shadowPath = shadowPath.cgPath
        self.imagesViewContainer.layer.shadowRadius = 15
        self.imagesViewContainer.BorderColor = UIColor.lightGray
        self.imagesViewContainer.backgroundColor = UIColor.white
        
        // Cuisine Card View UI Setup
        cuisineViewContainer.layer.cornerRadius = 15
        
        self.cuisineViewContainer.layer.masksToBounds = false
        self.cuisineViewContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.cuisineViewContainer.layer.shadowColor = UIColor.lightGray.cgColor
        self.cuisineViewContainer.layer.shadowOpacity = 0.4
        self.cuisineViewContainer.layer.shadowPath = shadowPath.cgPath
        self.cuisineViewContainer.layer.shadowRadius = 15
        self.cuisineViewContainer.BorderColor = UIColor.lightGray
        self.cuisineViewContainer.backgroundColor = UIColor.white
        
        // Total Deals Availed Card View UI Setup
        totalDealsAvailedContainer.layer.cornerRadius = 15
        
        let shadowPathTotalDeals = UIBezierPath(rect: CGRect(x: -shadowSize / 2.5,
                                                             y: -shadowSize / 2.5,
                                                             width: self.totalDealsAvailedContainer.frame.size.width + shadowSize,
                                                             height: self.totalDealsAvailedContainer.frame.size.height + shadowSize))
        self.totalDealsAvailedContainer.layer.masksToBounds = false
        self.totalDealsAvailedContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.totalDealsAvailedContainer.layer.shadowColor = UIColor.lightGray.cgColor
        self.totalDealsAvailedContainer.layer.shadowOpacity = 0.4
        self.totalDealsAvailedContainer.layer.shadowPath = shadowPathTotalDeals.cgPath
        self.totalDealsAvailedContainer.layer.shadowRadius = 15
        self.totalDealsAvailedContainer.BorderColor = UIColor.lightGray
        self.totalDealsAvailedContainer.backgroundColor = UIColor.white
        
        // Total Coupons Availed Card View UI Setup
        totalCouponsAvailedContainer.layer.cornerRadius = 15
        
        
        self.totalCouponsAvailedContainer.layer.masksToBounds = false
        self.totalCouponsAvailedContainer.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.totalCouponsAvailedContainer.layer.shadowColor = UIColor.lightGray.cgColor
        self.totalCouponsAvailedContainer.layer.shadowOpacity = 0.4
        self.totalCouponsAvailedContainer.layer.shadowPath = shadowPathTotalDeals.cgPath
        self.totalCouponsAvailedContainer.layer.shadowRadius = 15
        self.totalCouponsAvailedContainer.BorderColor = UIColor.lightGray
        self.totalCouponsAvailedContainer.backgroundColor = UIColor.white
        
        
    }
    

    func callStoreDashboardAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_DASHBOARD
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        let image = result!["response"]["imageCount"].int
                        let categorys = result!["response"]["category"].string
                        let tDeal = result!["response"]["totalDeal"].string
                        let tCount = result!["response"]["totalCoupons"].string
                        
                        self.imageCount.text = String(image!)
                        self.category.text = categorys
                        self.totalDeals.text = tDeal
                        self.totalCoupons.text = tCount
                        
                        
                       
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        
        }

    }
}
}
