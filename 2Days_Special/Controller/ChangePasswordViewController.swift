//
//  ChangePasswordViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 21/12/20.
//

import UIKit
import Alamofire

class ChangePasswordViewController: UIViewController {
    
    
    @IBOutlet weak var mcurrentPassword: UITextField!
    @IBOutlet weak var mNewPassword: UITextField!
    @IBOutlet weak var mConfirmPassword: UITextField!
    
    
    @IBOutlet weak var topBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI() {
        
        // TopBar View UI Setup
        topBar.clipsToBounds = true
        topBar.layer.cornerRadius = 33
        topBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
   
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveTap(_ sender: Any) {
        
        
        // validation
   
        
        if(mcurrentPassword.text?.count == 0 || !(mcurrentPassword.text!.count >= 6 && mcurrentPassword.text!.count <= 15) ){
            mcurrentPassword.setShadowColor(UIColor.red)
        }else{
            mcurrentPassword.setShadowColor(UIColor.lightGray)
        }
        
        if(mNewPassword.text?.count == 0 || !(mNewPassword.text!.count >= 6 && mNewPassword.text!.count <= 15) ){
            mNewPassword.setShadowColor(UIColor.red)
        }else{
            mNewPassword.setShadowColor(UIColor.lightGray)
        }
        
        if(mConfirmPassword.text?.count == 0 || !(mConfirmPassword.text!.count >= 6 && mConfirmPassword.text!.count <= 15) ){
            mConfirmPassword.setShadowColor(UIColor.red)
        }else{
            mConfirmPassword.setShadowColor(UIColor.lightGray)
        }
        
        
        
        
        
        
        if(mcurrentPassword.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill current password")
            return
        }
        else if(mNewPassword.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill new  password")
            return
        }
        else if(mConfirmPassword.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill confirm  password")
            return
        }
        else if(mNewPassword.text != mConfirmPassword.text)
        {
            
            ShowAlertView(controller: self, title: AppName, message: "Password doesnt match")
                return
            }
    else{
        
        
        
        // calling api
        let params = ["email_or_phone": UserDefaults.standard.string(forKey: "email")!,
                      "current_password": mcurrentPassword.text!,
                      "new_password" : mNewPassword.text!,
                      "confirm_password" : mConfirmPassword.text!,
                      "option": "update_password"]
        
        callUpdatePasswordAPI(params: params, isNeedToShowLoader: true )
    
    }
        
        
    }
    
    func callUpdatePasswordAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = UPDATE_PASSWORD
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    
                   
                    
                    let alert: UIAlertController = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        UIAlertAction in
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                              self.present(alert, animated: true, completion: nil)
                        
                
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
