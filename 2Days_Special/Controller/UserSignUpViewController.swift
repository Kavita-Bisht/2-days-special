//
//  uSignupViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 10/12/20.
//

import Foundation
import  UIKit
import Alamofire
//import GoogleSignIn


class UserSignUpViewController : UIViewController {
    
    @IBOutlet weak var viewHolder: UIView!
    
    // Signup Form Field outlets
    @IBOutlet weak var mName: UITextField!
    @IBOutlet weak var mEmail: UITextField!
    @IBOutlet weak var mPhone: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    @IBOutlet weak var termCheck: Checkbox!
    
    //signup outlets
    @IBOutlet weak var mSignUpButton: DesignableButton!
    
    
    @IBOutlet weak var mTermCondition: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupUI()
        
        
    }
    
    
    
    func setupUI(){
        
        // viewHolder cornerRadius & Shadow
        viewHolder.layer.cornerRadius = 45
        viewHolder.layer.shadowColor = UIColor.lightGray.cgColor
        viewHolder.layer.shadowOffset = CGSize(width: 1, height: -6)
        viewHolder.layer.shadowOpacity = 0.3
        
        //add botoom border to text field
        mName.setBottomBorder()
        mEmail.setBottomBorder()
        mPhone.setBottomBorder()
        mPassword.setBottomBorder()
        
        //border radius & shadow to login button
        mSignUpButton.layer.cornerRadius = 19
        let shadowSize : CGFloat = 1.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2.5,
                                                   y: -shadowSize / 2.5,
                                                   width: self.mSignUpButton.frame.size.width + shadowSize,
                                                   height: self.mSignUpButton.frame.size.height + shadowSize))
        self.mSignUpButton.layer.masksToBounds = false
        self.mSignUpButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.mSignUpButton.layer.shadowColor = UIColor(red: 208, green: 0, blue: 0, alpha: 1).cgColor
        self.mSignUpButton.layer.shadowOpacity = 0.2
        self.mSignUpButton.layer.shadowPath = shadowPath.cgPath
        self.mSignUpButton.layer.shadowRadius = 19
        
        // term & condition listener
        let termtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(termTap(tapGestureRecognizer:)))
       mTermCondition.isUserInteractionEnabled = true
        mTermCondition.addGestureRecognizer(termtapGestureRecognizer)
        
    }
    
    @objc func termTap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController;
        vc.ABOUT_OR_CONTACT_OR_TERM = "term"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpButtonTap(_ sender: Any) {
        
        
        // validation
        
        if termCheck.isChecked {
            
            
            if (mName.text?.count == 0)
            {
                mName.setShadowColor(UIColor.red)
            }else
            {
                mName.setShadowColor(UIColor.lightGray)
            }
            
            
            if (mEmail.text?.count == 0 || AppHelper.isValidEmail(testStr: mEmail.text!) == false)
            {
                mEmail.setShadowColor(UIColor.red)
            }else
            {
                mEmail.setShadowColor(UIColor.lightGray)
            }
            
            if(mPassword.text?.count == 0 || !(mPassword.text!.count >= 6 && mPassword.text!.count <= 15) ){
                mPassword.setShadowColor(UIColor.red)
            }else{
                mPassword.setShadowColor(UIColor.lightGray)
            }
            
            if(mPhone.text?.count == 0 || mPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil || mPhone.text?.count != 10 ){
                mPhone.setShadowColor(UIColor.red)
            }else{
                mPhone.setShadowColor(UIColor.lightGray)
            }
            
            
            if(mName.text?.count == 0){
                ShowAlertView(controller: self, title: AppName, message: "Please fill name")
                return
            }
            
            if(mEmail.text?.count == 0){
                ShowAlertView(controller: self, title: AppName, message: "Please fill email")
                return
            }
           
            else if(mPassword.text?.count == 0){
                ShowAlertView(controller: self, title: AppName, message: "Please fill password")
                return
            }
            else if(mPhone.text?.count == 0){
                ShowAlertView(controller: self, title: AppName, message: "Please fill phone number")
                return
            }
            else if(AppHelper.isValidEmail(testStr: mEmail.text!) == false){
                ShowAlertView(controller: self, title: AppName, message: "Email is incorrect")
                return
            }
            else if(mPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil || mPhone.text?.count != 10)
            {
                ShowAlertView(controller: self, title: AppName, message: "Phone no is incorrect")
                return
            }
            
            else if(!(mPassword.text!.count >= 6 && mPassword.text!.count <= 15))
            {
                ShowAlertView(controller: self, title: AppName, message: "Password must be between 6 to 15 characters")
                return
            }
           
        }
        else{
            ShowAlertView(controller: self, title: AppName, message: "Accept terms and conditions")
            return
        }
        
        
        
        // calling api
        let params = ["device_id" : UIDevice.current.identifierForVendor?.uuidString,
                      "device_type" : "2",
                      "device_token": "cj6zKwg7EhI:APA91bEImBmEJ6fq2pOHgPGKN5oaVEah0X2Xsu2ufz51BR6M5sbl5H_W-g4OmUSK8ol7WaPAtXYiC5tB-UKdJ8xjJekp0uHMtxgAlMuhyKxu1OK0eQL8AfmVUfsbLrZ305xQ5MjH8f5g",
                      "account_type": "1",
                      "email": mEmail.text!,
                      "name" : mName.text! ,
                      "password": mPassword.text!,
                      "phone": mPhone.text! ,
                      "option": "signup"]
        
        callUserSignUpAPI(params: params, isNeedToShowLoader: false)
    }
    
    @IBAction func loginInButtonTap(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    func callUserSignUpAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        
        
        if !AppHelper.isInterNetConnectionAvailable() {
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = USER_SIGN_UP
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                        UserModel.parse( result!["response"])
                        
                        // Navigate to UserHome
                        
                        
                        UserDefaults.standard.setValue("1", forKey: "is_login")
                        UserDefaults.standard.setValue("0", forKey: "user")
                        UserDefaults.standard.synchronize()
                        
                        let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
                        
                        self.navigationController!.pushViewController(homeVC, animated: true)
                        
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    
    
    
//    
//    @IBAction func facebookButtonTapped(_ sender: Any) {
//        
//        if !AppHelper.isInterNetConnectionAvailable() {
//            return;
//        }
//        
//        weak var weakSelf = self
//        
//        FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result, error) in
//            if result != nil{
//                let statusCode = result!["code"].stringValue
//                let message = result!["message"].string ?? kErrorMsg
//                
//                if statusCode == "200" {
//                    if result!["data"].dictionary != nil {
//                        
//                        
//                        
//                        UserModel.parse( result!["data"])
//                       
//                        UserDefaults.standard.set("facebook", forKey: "login_type")
//                        
//                        UserDefaults.standard.setValue("1", forKey: "is_login")
//                        UserDefaults.standard.synchronize()
//                        
//                        let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
//                        
//                        self.navigationController!.pushViewController(homeVC, animated: true)
//                        
//                    }
//                }
//                else{
//                    self.ShowAlertView(title: AppName, message: message)
//                }
//            }
//            else{
//                self.ShowAlertView(title: AppName, message: kErrorMsg)
//            }
//        }
//    }
    
    
//    @IBAction func googleButtonTapped(_ sender: Any) {
//        
//        if !AppHelper.isInterNetConnectionAvailable() {
//            return;
//        }
//        weak var weakSelf = self
//        
//        GoogleLoginManager.sharedInstance.callGoogleSignIn{ (result, error) in
//            
//            if (result != nil){
//                let statusCode = result!["code"].stringValue
//                let message = result!["message"].string ?? kErrorMsg
//                
//                if statusCode == "200" {
//                    if result!["data"].dictionary != nil {
//                        UserModel.parse( result!["response"])
//                       
//                        UserDefaults.standard.set("google", forKey: "login_type")
//                        
//                        UserDefaults.standard.setValue("1", forKey: "is_login")
//                        UserDefaults.standard.synchronize()
//                   
//                        let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
//                        
//                        self.navigationController!.pushViewController(homeVC, animated: true)
//                        
//                    }
//                }
//                else{
//                    weakSelf?.ShowAlertView(title: AppName, message: message)
//                }
//            }
//            else{
//                weakSelf?.ShowAlertView(title: AppName, message: kErrorMsg)
//            }
//        }
//    }
    
    
    
}

extension UITextField {
    func setBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.6
    }
    
    
    
    
    func setShadowColor(_ color: UIColor){
        self.layer.shadowColor = color.cgColor
    }
    
    
}
