//
//  HomeViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 14/12/20.
//

import UIKit
import SideMenu
import SwiftyJSON
import Alamofire
import GoogleMaps

class UserHomeViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var searchTextField: DesignableTextField!
    
    @IBOutlet weak var profileImageView: DesignableImage!
    
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var listButton: UIButton!
    
    @IBOutlet weak var foodCategoryCollectionView: UICollectionView!
    
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    
    
    @IBOutlet weak var restaurantCollectionView: UICollectionView!
    
    var storeList:[StoreDetailModel] = []
    
    var categoryList:[CategoryModel] = []
    
    var bannerList:[BannerModel] = []
    
    @IBOutlet weak var mapContainer: UIView!
    var mapView:GMSMapView = GMSMapView()
    
    
 
    
    var locationManager = CLLocationManager()
    
    var state : String = ""
    
    var city : String = ""
    
    var country : String = ""
    
    var latitude : String = ""
    
    var longitude : String = ""
    
    
    @IBOutlet weak var mCurrentLoction: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        
        // CHECKING FOR LOGIN SCREEN
        let loginScreen :String = UserDefaults.standard.string(forKey: "is_login") ?? "0"
        
        if loginScreen == "0"
        {
            
            _ = UIApplication.shared.delegate as! AppDelegate
            
            //  _ = self.view.window?.windowScene?.delegate
            let customView = SubUIView(frame: self.view.frame)
            self.view.window?.addSubview(customView)
            customView.changeBounds()
           
            
        }
        
        
        realtimeLocation()
        
        // calling api
        let params = [
                      "option": "categories"]
        
        print(params)
        
        callCategoryAPI(params: params as Parameters, isNeedToShowLoader: false)
        
        
        
        
    }
    
    func setMapAtLocation(loc:CLLocation){
        
        // maps
        let camera = GMSCameraPosition.camera(withLatitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude, zoom: 16)
         mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)

        self.mapContainer.addSubview(mapView)
        
     
        // GOOGLE MAPS SDK: COMPASS
                mapView.settings.compassButton = true

                // GOOGLE MAPS SDK: USER'S LOCATION
               mapView.isMyLocationEnabled = true
                mapView.settings.myLocationButton = true
              

       
                // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
                //marker.title = "Dehradun"
                //marker.snippet = "My Location"
                marker.map = mapView
        
    }
    
    
    func realtimeLocation() {
        
        // reuquest for enabling location permission
        locationManager.requestWhenInUseAuthorization()
        
        // callback delgegate to get updated location
        locationManager.delegate = self
        
        // asking for updating location
        locationManager.startUpdatingLocation()
        
        
    }
    
    // callback when location update
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            latitude = String(location.coordinate.latitude)
            longitude = String(location.coordinate.longitude)
            
            // calling api
            let params = [ "userId":UserDefaults.standard.string(forKey: "user_id"),
                           "option":"fetchNearByStores",
                           "latitude":String(latitude),
                           "longitude":String(longitude),
                           "cat_id":"3"]
            
            print(UserDefaults.standard.string(forKey: "user_id"), latitude, longitude)
            
            callStoreNearbyAPI(params: params, isNeedToShowLoader: true )
            
            setMapAtLocation(loc: location)
            
            
            let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(location,
                                            completionHandler: { (placemarks, error) in
                                                if error == nil {
                                                    let firstLocation = placemarks?[0]
                                                    
                                                    print(firstLocation?.country)  // United States
                                                    self.country = firstLocation?.country ?? ""
                                                    
                                                   
                                    
                                                    self.city =   firstLocation?.locality ?? "" //Dehradun
                                                    self.state = firstLocation?.administrativeArea ?? "" //UT
                                                    
                                                    self.mCurrentLoction.text = self.city + ", " + self.country
                                                   
                                                }
                                                else {
                                                    
                                                    print("No location info")
                                                    
                                                }
                                            })
        }
        else{
            // No location was available.
            
            print("No location info")
            
        }
        
    }
    
    
    
    func callCategoryAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_CATEGORY
        
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { [self] (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].arrayValue != nil {
                        
                        print(result!["response"])
                        
                        categoryList = []
                        
                      
                        
                        for i in result!["response"].arrayValue {
                            let category = CategoryModel()
                            category.parse(i)
                            
                            self.categoryList.append(category)
                        }
                        
                        
                        foodCategoryCollectionView.reloadData()
                     
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    @IBAction func optionMapTap(_ sender: Any) {
    
        mapContainer.isHidden = false
        listButton.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        locationButton.backgroundColor = UIColor(red: 255, green: 223, blue: 230, alpha: 1)
    
    }
    
    
    @IBAction func optionListTap(_ sender: Any) {
        mapContainer.isHidden = true
        locationButton.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        listButton.backgroundColor = UIColor(red: 255, green: 223, blue: 230, alpha: 1)
    }
    
    func setupUI(){
        
        // SEARCH TEXT FIELD
        searchTextField.layer.cornerRadius = 18
        
        // bgcolor option map / option list (default)
        listButton.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        locationButton.backgroundColor = UIColor(red: 255, green: 223, blue: 230, alpha: 1)
        
        // LOCATION VIEW BUTTON
        locationButton.clipsToBounds = true
        locationButton.layer.cornerRadius = 14
        locationButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        // LIST VIEW BUTTON
        listButton.clipsToBounds = true
        listButton.layer.cornerRadius = 14
        listButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        // PROFILE IMAGE VIEW
        profileImageView.layer.cornerRadius = 20
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
//        profileImageView.isUserInteractionEnabled = true
//        profileImageView.addGestureRecognizer(tapGestureRecognizer)
        
        // FOODCATEGORY COLLECTION
        
        // 1. delegate
        foodCategoryCollectionView.dataSource = self
        foodCategoryCollectionView.delegate = self
        
        // 2. cell
        foodCategoryCollectionView.register(UINib.init(nibName: "FoodCategoryCell", bundle: nil), forCellWithReuseIdentifier: "FoodCategoryReusableCell")
        
        // 3. data
        
        // additional configuration
        foodCategoryCollectionView.showsHorizontalScrollIndicator = false
        if let layout = foodCategoryCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }

            
            // BANNER COLLECTION
            bannerCollectionView.dataSource = self
            bannerCollectionView.delegate = self
            
            // 2. cell
            bannerCollectionView.register(UINib.init(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerReusableCell")
            
          
            
            // additional configuration
            foodCategoryCollectionView.showsHorizontalScrollIndicator = false
            if let layout = foodCategoryCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            
            
            
            
            // RESTAURANT COLLECTION
            
            // 1. delegate
            restaurantCollectionView.dataSource = self
            restaurantCollectionView.delegate = self
            
            // 2. cell
            restaurantCollectionView.register(UINib.init(nibName: "RestaurantCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantReusableCell")
            
            // 3. data

        
    }
    
    
    func callStoreNearbyAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = STORE_NEARBY
        
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    if result!["response"].dictionary != nil {
                        
                        print(result!["response"])
                        
                        
                        for i in result!["response"]["storeData"].arrayValue {
                            let store = StoreDetailModel()
                            store.parse(i)
                            self.storeList.append(store)
                        }
                        
                        for i in result!["response"]["banners"].arrayValue {
                            let banner = BannerModel()
                            banner.parse(i)
                            self.bannerList.append(banner)
                        }
                        
                        for i in result!["response"]["categories"].arrayValue {
                            let category = CategoryModel()
                            category.parse(i)
                            self.categoryList.append(category)
                        }
                        
                        
                        self.restaurantCollectionView.reloadData()
                        self.bannerCollectionView.reloadData()
                       // self.foodCategoryCollectionView.reloadData()
                     
                        
                    }
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
  
    
    
    @objc func profileImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController;            self.navigationController?.pushViewController(vc, animated: true)
        // Your action
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     
     
     }
     */
    
}

// Food Category/ Restaurant Collection View Adapter
extension UserHomeViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == foodCategoryCollectionView{
            print(categoryList.count)

            return categoryList.count
        }
        else if collectionView == restaurantCollectionView{
            print(storeList.count)
            return storeList.count
        }
        else if collectionView == bannerCollectionView{
            return bannerList.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == foodCategoryCollectionView{
            
            let cell = foodCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "FoodCategoryReusableCell", for: indexPath) as! FoodCategoryCell
            cell.configure(with: categoryList[indexPath.row])
            
            return cell
        }
        else if collectionView == restaurantCollectionView{
            
            
            let cell = restaurantCollectionView.dequeueReusableCell(withReuseIdentifier: "RestaurantReusableCell", for: indexPath) as! RestaurantCell
             cell.configure(with: storeList[indexPath.row])
            return cell
        }
        else if collectionView == bannerCollectionView{
            
            
            let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "BannerReusableCell", for: indexPath) as! BannerCollectionViewCell
             cell.configure(with: bannerList[indexPath.row])
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    // item size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == restaurantCollectionView
        {
            let numberOfItemsPerRow = 2
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
            
            let size = Int((collectionView.bounds.width - totalSpace - 20) / CGFloat(numberOfItemsPerRow))
            
            return CGSize(width: size, height: size)
        }
        else if collectionView == foodCategoryCollectionView
        {
            return CGSize(width: 100, height: 30)
        }
        else if collectionView == bannerCollectionView {
            let cellsize = CGSize(width: UIScreen.main.bounds.width - 50 , height: collectionView.frame.height - 22)
            
            return cellsize
        }
        
        return CGSize(width: 0, height: 0)
        
    }
    
    
        // item spacing = vertical spacing in horizontal flow
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
        }
    
        // line spacing = horizontal spacing in horizontal flow
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
        }
    
    
    // edge insets
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == restaurantCollectionView {
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController;
            vc.store_id = storeList[indexPath.row].store_id
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
        }
    }
    
    
}


