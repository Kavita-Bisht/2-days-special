//
//  EditProfileViewController.swift
//  2Days_Special
//
//  Created by Ganesh Bisht on 21/12/20.
//

import UIKit
import Alamofire

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var profileImageView:
        DesignableImage!
    
    
    @IBOutlet weak var mFullName: UITextField!
    
    @IBOutlet weak var mEmail: UITextField!
    
    @IBOutlet weak var mPhone: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI() {
        
        // TopBar View UI Setup
        topBar.clipsToBounds = true
        topBar.layer.cornerRadius = 33
        topBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        // corner radius profile image
        profileImageView.layer.cornerRadius = profileImageView.bounds.size.width / 2
        
        // fetch user info from preferences
        mFullName.text =  UserDefaults.standard.string(forKey: "name")
        mEmail.text = UserDefaults.standard.string(forKey: "email")
        mPhone.text = UserDefaults.standard.string(forKey: "phone")
    }
    
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func updateTap(_ sender: Any) {
        
        // validation
        if mFullName.text?.count == 0
        {
            mFullName.setShadowColor(UIColor.red)
        }else
        {
            mFullName.setShadowColor(UIColor.lightGray)
        }
        
        
        if mEmail.text?.count == 0 ||  AppHelper.isValidEmail(testStr: mEmail.text!) == false
        {
            mEmail.setShadowColor(UIColor.red)
        }else
        {
            mEmail.setShadowColor(UIColor.lightGray)
        }
        
        
        
        if(mPhone.text?.count == 0 || !(mPhone.text!.count == 10)){
            mPhone.setShadowColor(UIColor.red)
        }else{
            mPhone.setShadowColor(UIColor.lightGray)
        }
        
        
        
        
        
        
        if(mFullName.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill name")
            return
        }
        else if(mEmail.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill email id")
            return
        }
        else if(mPhone.text?.count == 0){
            ShowAlertView(controller: self, title: AppName, message: "Please fill phone no")
            return
        }
        else if(mEmail.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil && AppHelper.isValidEmail(testStr: mEmail.text!) == false){
            ShowAlertView(controller: self, title: AppName, message: "Email is incorrect")
            return
        }
        else if((mPhone.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil) )
        {
            if( mPhone.text?.count != 10) {
            ShowAlertView(controller: self, title: AppName, message: "Phone no is incorrect")
                return
            }
            
        }
        
      
        
        
        
        
        // calling api
        let params = ["email_or_phone": mPhone.text!,
                      "name": mFullName.text!,
                      "option": "update_profile"]
        
        callProfileUpdateAPI(params: params, isNeedToShowLoader: true )
        
    }
    
    func callProfileUpdateAPI(params: Parameters, isNeedToShowLoader: Bool) {
        
        if !AppHelper.isInterNetConnectionAvailable() {
            self.ShowAlertView(controller: self,title: AppName, message: "No internet connection")
            return ;
        }
        
        if isNeedToShowLoader{
            AppHelper.sharedInstance.displaySpinner()
        }
        
        let urlString = UPDATE_PROFILE
        
        
        APIManager.performPOSTRequest(urlString: urlString, andParameters: params) { (result, error) in
            
            AppHelper.sharedInstance.removeSpinner()
            
            if result != nil{
                
                let statusCode = result!["status"].stringValue
                
                let message = result!["message"].string ?? kErrorMsg
                
                print(message)
                
                if statusCode == "200" {
                    
                    
                    print(result!["response"])
                    
                    
                    UserDefaults.standard.set(self.mFullName.text, forKey: "name")
                    
                    let alert: UIAlertController = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        UIAlertAction in
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                    self.present(alert, animated: true, completion: nil)
              
        
                    
                }
                else{
                    self.ShowAlertView(controller: self,title: AppName, message: message)
                    
                }
            }
            else{
                self.ShowAlertView(controller: self,title: AppName, message: kErrorMsg)
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
